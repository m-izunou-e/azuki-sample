<?php
/**
 * CSVファイルでのデータの一括登録などの処理に関する設定を記述します
 *
 * 実際に処理を行うクラスの定義が必須です
 * 他設定は処理を実装したクラスの実装に依存して自由に定義してください
 * csvでのデータ登録・ダウンロードについては、sample_csvのようにcolumnの設定が必要です。
 *
 */

return [
    'queue_kind' => [
        'sample_csv',
    ],
    'sample_csv' => [
        'type_name' => 'サンプルCSV一括登録',
        'class'     => \AzukiSample\App\Services\Executer\SampleCsvExecuter::class,
        'columns'   => [
            [
                'column' => 'id',
                'header' => 'ID',
            ],
            [
                'column' => 'name',
                'header' => '名前',
            ],
            [
                'column' => 'email',
                'header' => 'メールアドレス',
            ],
            [
                'colName' => 'zipcode',
                'column'  => ['zipcode1', 'zipcode2'],
                'header'  => '郵便番号',
            ],
            [
                'column' => 'pref',
                'header' => '都道府県',
            ],
            [
                'column' => 'address1',
                'header' => '市区町村',
            ],
            [
                'column' => 'address2',
                'header' => '番地以下',
            ],
            [
                'column' => 'gender',
                'header' => '性別',
            ],
            [
                'column' => 'birth',
                'header' => '生年月日',
            ],
            [
                'column' => 'role',
                'header' => '権限',
            ],
            [
                'column' => 'is_login',
                'header' => 'ログイン可否',
            ],
            [
                'column' => 'enable',
                'header' => '有効・無効',
            ],
        ],
    ],
];
