<?php
return [
    'namespace' => '\AzukiSample\App\Contracts\Http\Controllers',
    'group' => [
        [
            'prefix'     => 'manage',
            'middleware' => ['auth.manage', 'acl'],
            'list'       => [
                [   // 管理画面サンプル画面
                    'prefix'     => 'sample',
                    'set'        => [
                        [ 'method' => 'get',  'url' => 'index' ],
                        [ 'method' => 'get',  'url' => 'list' ],
                        [ 'method' => 'post', 'url' => 'list' ],
                        [ 'method' => 'post', 'url' => 'regist' ],
                        [ 'method' => 'get',  'url' => 'regist' ],
                        [ 'method' => 'get',  'url' => 'detail/{id}', 'where' => ['id' => '[0-9]+'] ],
                        [ 'method' => 'post', 'url' => 'edit' ],
                        [ 'method' => 'get',  'url' => 'edit/{id}', 'where' => ['id' => '[0-9]+'] ],
                        [ 'method' => 'get',  'url' => 'edit/{id}/prev/{prev}', 'where' => ['id' => '[0-9]+', 'prev' => 'detail|list'] ],
                        [ 'method' => 'post', 'url' => 'confirm' ],
                        [ 'method' => 'post', 'url' => 'done' ],
                        [ 'method' => 'post', 'url' => 'enable' ],
                        [ 'method' => 'post', 'url' => 'delete' ],
                        [ 'method' => 'post', 'url' => 'csv-download' ],
                    ],
                    'controller' => 'SampleController',
                ],
            ],
        ],
    ],
];
