<?php
namespace AzukiSample;

use Azuki\App\Support\ServiceProvider as AzukiServiceProvider;
use Azuki\App\Services\RouteControll;

/**
 *
 *
 *
 */
class ServiceProvider extends AzukiServiceProvider
{
    /**
     * azukiで使用している設定ファイルのリスト
     *
     */
    protected $configList = [
        'azuki.addon'        => ['isPublish' => false, 'merge' => self::CONF_MERGE_TYPE_NRL],
        'azuki.execute_type' => ['isPublish' => true,  'merge' => self::CONF_MERGE_TYPE_NRL],
    ];

    /**
     * azukiの基本システムを構成しているコントローラ、モデル、バリデーター
     * のリスト
     * ここで定義されているキー名に一意なプレフィックスをつけた名前で各クラスを
     * コンテナに登録します
     *
     */
    protected $baseServiceClassList = [
        'controller' => [
            'manage.sample'         => 'Manage\\SampleController',
        ],
        'model' => [
            'sample'                 => 'Sample',
        ],
        'validator' => [
            'manage.sample'         => 'Manage\\SampleValidator',
        ],
    ];
    
    /**
     *
     *
     */
    protected $publishVendorName = 'azuki';
    
    /**
     *
     *
     */
    protected function getVendorPath()
    {
        return __DIR__.'/../';
    }
    
    /**
     *
     *
     */
    protected function registerConfigFiles()
    {
        parent::registerConfigFiles();

        $configPath = $this->getConfigPath();
        $menuConfigFile = $configPath . '/azuki.menu_list.php';
        $menu = require $menuConfigFile;

        if (!$this->app->configurationIsCached()) {
            if(isset($menu['system'])) {
                $this->app['config']->set(
                    'azuki.menu_list.system', array_merge(
                        $this->app['config']->get('azuki.menu_list.system', []),
                        $menu['system']
                ));
            }
            if(isset($menu['manage'])) {
                $this->app['config']->set(
                    'azuki.menu_list.manage', array_merge(
                        $this->app['config']->get('azuki.menu_list.manage', []),
                        $menu['manage']
                ));
            }
            
            // contentsの設定を追加
            $this->app['config']->set(
                'azuki.standard.contents', array_merge(
                    $this->app['config']->get('azuki.standard.contents', []),
                    ['sample' => env( 'AZUKI_CONTENTS_SAMPLE', true) === true  ? 'enabled' : 'disabled',]
            ));
        }
    }
    
    /**
     *
     *
     *
     */
    protected function getRouteControllerInstance($conf)
    {
        return  new RouteControll($conf);
    }
    
    /**
     * function setPublishes
     *
     * vendor:publishでパブリッシュするファイルを設定する
     *
     */
    protected function setPublishes()
    {
    }
    
    /**
     *
     *
     */
    protected function addVendorRouting()
    {
        return $this->app['config']->get('azuki.standard.routing');
    }
    
    /**
     *
     *
     */
    protected function routeFileList()
    {
        return [
            'azuki.web',
        ];
    }
}
