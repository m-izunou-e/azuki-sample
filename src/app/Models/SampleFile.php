<?php
namespace AzukiSample\App\Models;

/**
 * 管理画面用のアカウント管理のモデル
 *
 * @copyright Copyright 2018 Azuki Project.
 * @author    Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */

use Azuki\App\Models\BaseHardDeleteModel as Model;

/**
 * class Manager
 *
 */
class SampleFile extends Model
{
    /**
     * The database table account by the model.
     *
     * @var string
     */
    protected $table = 'azuki_sample_file';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'sample_id',
        'file_id_name',
        'column_name',
        'order',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     *
     *
     *
     */
    protected static function boot() {
        parent::boot();

        self::deleted(function($obj) {
            // 画像削除
            $file = $obj->file_id_name;
            parent::deleteFileData($file);
        });
    }
    
    /**
     * function getTableName
     *
     * オペレーションログに記録する操作テーブル名を返す
     * 本クラスにおいては関連テーブルとの紐づきを表現するために以下のような形で
     * 返すようにオーバーライドしている
     *
     */
    protected function getTableName($obj)
    {
        return sprintf('%s (id:%s)->%s', 'azuki_sample', $obj->sample_id, $obj->getTable());
    }
}
