<?php
namespace AzukiSample\App\Models;

/**
 * サンプルページ用のモデル
 *
 * ユーザー情報管理のような画面構成が諸々の機能サンプルとして適しているので
 * そのようなデータ管理をする構成にしている
 *
 * @copyright Copyright 2018 Azuki Project.
 * @author    Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */

use Azuki\App\Models\AbstractBelongs as Model;

use AzukiSample\App\Contracts\Models\Sample as ContractsInterfase;

/**
 * class Manager
 *
 */
class Sample extends Model implements ContractsInterfase
{
    /**
     * The database table account by the model.
     *
     * @var string
     */
    protected $table = 'azuki_sample';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'zipcode',
        'pref',
        'address1',
        'address2',
        'gender',
        'birth',
        'role',
        'is_login',
        'enable',
        'contents',
        'contents_html',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];
    
    
    /**
     *
     *
     */
    public function sampleFile()
    {
        return $this->hasMany(SampleFile::class, 'sample_id', 'id')->orderBy('order');
    }
    
    /**
     * function _create
     *
     * @param Array $data 登録する配列
     */
    public function _create(array $data)
    {
        $data['zipcode'] = $this->getZipCode($data);
    
        \DB::beginTransaction();
        try {
            $ret = parent::_create($data);
            $sampleId = $ret->id;
            $this->replaceFileData($sampleId, $data);

            \DB::commit();
        } catch(\Exception $e) {

            \DB::rollback();
            $this->exceptionLog($e);

            $ret = false;
        }

        return $ret;
    }
    
    /**
     * function _update
     *
     * @param Integer $id 対象データの識別子
     * @param Array   $data 更新する配列
     */
    public function _update($id, array $data)
    {
        $data['zipcode'] = $this->getZipCode($data);

        \DB::beginTransaction();
        try {
            $ret = parent::_update($id, $data);
            $this->replaceFileData($id, $data);

            \DB::commit();
        } catch(\Exception $e) {

            \DB::rollback();
            $this->exceptionLog($e);

            $ret = false;
        }

        return $ret;
    }
    
    /**
     *
     *
     */
    protected function getZipCode($data)
    {
        if( !isset($data['zipcode']) || empty($data['zipcode']) ) {
            $data['zipcode'] = $data['zipcode1'] . $data['zipcode2'];
        }
        
        return $data['zipcode'];
    }
    
    /**
     *
     *
     */
    public function delete()
    {
        $ret = parent::delete();
        try {
            foreach($this->sampleFile as $file) {
                $file->delete();
            }
        } catch(\Exception $e) {
            $this->exceptionLog($e);
        }
        return $ret;
    }
    
    /**
     *
     *
     */
    private function replaceFileData($id, $data)
    {
        $fileColumns = [
            'thumbnail',
            'movie',
        ];

        // ファイル登録
        $sampleFileModel = new SampleFile();
        foreach( $fileColumns as $col ) {
            $newFilesName = [];
            $query = $sampleFileModel
                ->where('sample_id',   '=', $id)
                ->where('column_name', '=', $col);
            $sampleFile = $query->get(); // 後で差分確認するために取得している
            $query->delete(); // 画像データの削除をしないためにBuilderのdeleteで実行

            if(isset( $data[$col] ) && !empty( $data[$col] )) {
                $data[$col] = is_array($data[$col]) ? $data[$col] : [$data[$col]];
                foreach( $data[$col] as $order => $name ) {
                    $idName = $this->getFileIdName($col, $id, $name);
                    $newFilesName[] = $idName;

                    $fileData = [
                        'sample_id'    => $id,
                        'file_id_name' => $idName,
                        'column_name'  => $col,
                        'order'        => $order,
                    ];
                    $ret = $sampleFileModel->_create($fileData);
                }
            }
            foreach($sampleFile as $file) {
                if( !in_array($file->file_id_name, $newFilesName) ) {
                    // 削除・上書きされた画像なので画像データの削除を行う
                    parent::deleteFileData($file->file_id_name);
                }
            }
        }
    }

    /**
     * function setCondition
     *
     * @param  QueryBuilder $query
     * @param  Array        $condition
     * @return QueryBuilder $query
     *
     */
    public function setCondition($query, $condition)
    {
        $query = parent::setCondition($query, $condition);


        $query = $this->when($query, [$condition, 'id'], function($query, $value) {
            return $query->where('id', $value);
        });
        $query = $this->when($query, [$condition, 'free'], function($query, $value) {
            $where = "(name LIKE ? or email LIKE ?)";
            $whereCond = array(
                "%".$value."%",
                "%".$value."%",
            );
            return $query->whereRaw($where, $whereCond);
        });
        $query = $this->when($query, [$condition, 'name'], function($query, $value) {
            $where = "name LIKE ?";
            $whereCond = array(
                "%".$value."%",
            );
            return $query->whereRaw($where, $whereCond);
        });
        $query = $this->when($query, [$condition, 'email'], function($query, $value) {
            $where = "email LIKE ?";
            $whereCond = array(
                "%".$value."%",
            );
            return $query->whereRaw($where, $whereCond);
        });
        $query = $this->when($query, [$condition, 'login_id'], function($query, $value) {
            $where = "login_id LIKE ?";
            $whereCond = array(
                "%".$value."%",
            );
            return $query->whereRaw($where, $whereCond);
        });
        $query = $this->when($query, [$condition, 'role'], function($query, $value) {
            return $query->whereIn('role', $value);
        });
        $query = $this->when($query, [$condition, 'enable'], function($query, $value) {
            return $query->whereIn('enable', $value);
        });
        $query = $this->when($query, [$condition, 'free_word'], function($query, $value) {
            $where = "(name LIKE ? OR email LIKE ?)";
            $whereCond = array(
                "%".$value."%",
                "%".$value."%",
            );
            return $query->whereRaw($where, $whereCond);
        });

        $query = $this->when($query, [$condition, 'zipcode'], function($query, $value) {
            $where = "zipcode LIKE ?";
            $whereCond = array(
                "%".$value."%",
            );
            return $query->whereRaw($where, $whereCond);
        });
        $query = $this->when($query, [$condition, 'pref'], function($query, $value) {
            return $query->where('pref', '=', $value);
        });
        $query = $this->when($query, [$condition, 'address1'], function($query, $value) {
            $where = "address1 LIKE ?";
            $whereCond = array(
                "%".$value."%",
            );
            return $query->whereRaw($where, $whereCond);
        });
        $query = $this->when($query, [$condition, 'address2'], function($query, $value) {
            $where = "address2 LIKE ?";
            $whereCond = array(
                "%".$value."%",
            );
            return $query->whereRaw($where, $whereCond);
        });
        $query = $this->when($query, [$condition, 'birth'], function($query, $value) {
            return $query->where('birth', '=', $value);
        });
        $query = $this->when($query, [$condition, 'gender'], function($query, $value) {
            return $query->where('gender', '=', $value);
        });

        return $query;
    }

    /**
     * function _findOrFail
     *
     *
     * @param Integer $id 取得するデータの識別子
     */
    public function _findOrFail($id)
    {
        $fileColumns = [
            'thumbnail',
            'movie',
        ];

        $rec = parent::_findOrFail($id);
        $sampleFiles = $rec->sampleFile;
        
        foreach( $fileColumns as $col ) {
            $tmp = [];
            foreach( $sampleFiles as $file ) {
                if( $col == $file->column_name ) {
                    $tmp[] = $file->file_id_name;
                }
            }
            $rec[$col] = $tmp;
        }
        $rec['zipcode1'] = $rec->zipcode1;
        $rec['zipcode2'] = $rec->zipcode2;
        
        return $rec;
    }
    
    /**
     *
     *
     */
    public function getSqlForOutputCsvData($cond)
    {
        $query = $this->setCondition( $this->prepare(), $cond );
        return $query;
    }

    /**
     *
     *
     */
    public function getAddressAttribute()
    {
        $pref = getSelectName('pref', $this->pref);
        return sprintf('%s-%s %s %s%s', $this->zipcode1, $this->zipcode2, $pref, $this->address1, $this->address2);
    }

    /**
     *
     *
     */
    public function getZipcode1Attribute()
    {
        return substr($this->zipcode, 0, 3);
    }

    /**
     *
     *
     */
    public function getZipcode2Attribute()
    {
        return substr($this->zipcode, 3, 4);
    }
}
