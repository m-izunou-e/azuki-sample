<?php
namespace AzukiSample\App\Http\Validation\Manage;
/**
 * 管理画面アカウントのバリデーションを定義したクラス
 *
 * エラー時のリダイレクトURL
 * 属性値の日本語表記の設定
 * バリデーションルールを記載する
 *
 */

use Azuki\App\Http\Validation\Validator;
use AzukiSample\App\Contracts\Http\Validation\Manage\SampleValidator as ContractsInterface;

class SampleValidator extends Validator implements ContractsInterface
{
    
    /**
     * attributeの初期化を行うかどうか
     * 各Validatorで個別に設定する場合はfalseにする
     */
    protected $isInitialAttribute = false;

    protected $validate_attributes = [
        'name'          => '名前',
        'email'         => 'メールアドレス',
        'password'      => 'パスワード',
        'password_conf' => 'パスワード(確認)',
        'role'          => '役割',
        'is_login'      => 'ログイン可否',
        'enable'        => '有効・無効',
        'zipcode1'      => '郵便番号(上３桁)',
        'zipcode2'      => '郵便番号(下４桁)',
        'pref'          => '都道府県',
        'address1'      => '市区町村',
        'address2'      => '番地・建物名以下',
        'gender'        => '性別',
        'birth'         => '生年月日',
    ];
    
    /**
     * messagesの初期化を行うかどうか
     * 各Validatorで個別に設定する場合はfalseにする
     */
    protected $isInitialMessages = false;

    protected $validate_messages = [
        'email.unique_on_belong' => 'その:attributeは指定の所属にてすでに登録されています',
    ];

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules($data, $flow = null, $id = null)
    {
//        $emailFormat = 'required|email|unique:azuki_sample,email,%s,id,deleted_at,NULL';
        $emailFormat = 'required|email|uniqueOnBelong:\\AzukiSample\\App\\Models\\Sample,email,%s,%s';
        $belong = NULL;
        if(supportOrganizations()) {
            $belong = isset($data['belong']) ? $data['belong'] : getLoginUserBelong();
        }
        $pwValid = 'alpha_num_j|min:8|max:24';
        $pwConValid = $pwValid.'|same:password';
        $role    = implode(',', getSelectValues('managerRoles'));
        $isLoign = implode(',', getSelectValues('enableLoginLabel'));
        $enable  = implode(',', getSelectValues('validManagerLabel'));
        $pref    = implode(',', getSelectValues('pref'));
        $gender  = implode(',', getSelectValues('gender'));

        $ret = [
            'name'          => 'required|max:20',
            'email'         => sprintf($emailFormat, $belong, $id),
            'password'      => 'nullable|'.$pwValid,
            'password_conf' => 'nullable|'.$pwConValid,
            'role'          => 'required|in:'.$role,
            'is_login'      => 'required|in:'.$isLoign,
            'enable'        => 'required|in:'.$enable,
            'zipcode1'      => 'required|regex:/^[0-9]{3}$/',
            'zipcode2'      => 'required|regex:/^[0-9]{4}$/',
            'pref'          => 'required|in:'.$pref,
            'address1'      => 'required',
//            'address2'      => '',
            'gender'        => 'required|in:'.$gender,
            'birth'         => 'required|date',
        ];

        if($flow == "regist") {
            $ret['password']      = 'required|' . $pwValid;
            $ret['password_conf'] = 'required|' . $pwConValid;
        }
        return $ret;
    }

}
