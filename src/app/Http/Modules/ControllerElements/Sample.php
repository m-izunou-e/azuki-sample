<?php
namespace AzukiSample\App\Http\Modules\ControllerElements;

/**
 * コントローラーに紐づくエレメントのセットを定義するトレイト
 *
 * CSVアップロード処理を持つ機能など、コントローラー以外の場所で
 * コントローラーと同様のバリデーションを行う必要があるような場合に
 * 本トレイトのようにエレメント要素を切り出したクラスを作成することで
 * 定義部分を共通化し、メンテナス性を向上させるための仕組み
 */


trait Sample
{
    /**
     * 画面構成に必要な要素のモジュールの定義
     *
     */
    protected $elementModues = [
        \Azuki\App\Http\Modules\Forms\BasicElements::class,
        \Azuki\App\Http\Modules\Forms\SimplePersonal::class,
        \Azuki\App\Http\Modules\Forms\UploadFiles::class,
        \Azuki\App\Http\Modules\Forms\WisyWig::class,
    ];

    /**
     * 各画面タイプごとの要素のオーダー設定
     *
     * array $elementsOrder
     */
    protected $elementsOrder = [
        'search' => ['free', 'address', 'birth', 'gender', 'role' ],
        'detail' => ['id', 'created', 'contents', 'name', 'email', 'address', 'gender', 'birth', 'role', 'isLogin', 'enable', 'thumbnail', 'movie' ],
        'form'   => ['contents', 'name', 'email', 'password', 'password_conf', 'address', 'gender', 'birth', 'role', 'isLogin', 'enable', 'thumbnail', 'movie' ],
        'list'   => ['id', 'name', 'email', 'address', 'role', 'isLogin', 'created', 'enable', 'ctrl', ],
    ];
}
