<?php
namespace AzukiSample\App\Http\Controllers\Manage;

/**
 *
 * @copyright Copyright 2018 Azuki Project.
 * @author    Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */

use Azuki\App\Http\Controllers\Manage\BaseController;
use Azuki\App\Http\Modules\ActionTrait\BaseActionPackageWithDetail;
use Azuki\App\Http\Modules\ActionTrait\TraitParts\TraitCsvDownloadAction;
use Azuki\App\Http\Interfaces\CsvDownloadInterface;

use AzukiSample\App\Contracts\Http\Validation\Manage\SampleValidator as Validator;
use AzukiSample\App\Contracts\Models\Sample as Model;
use AzukiSample\App\Http\Modules\ControllerElements\Sample as SampleElements;

/**
 * class IndexController
 *
 * 管理者ユーザーを実装するクラス
 */
class SampleController extends BaseController implements CsvDownloadInterface
{
    /**
     * サポートサービスのオブジェクト
     *
     * @var CtrlCupporter ctrlSupporter
     */
    protected $ctrlSupporter;

    /**
     * 基本のアクションセットのトレイトを読み込みます
     */
    use BaseActionPackageWithDetail;

    /**
     * CSVダウンロードのトレイトを読み込みます
     */
    use TraitCsvDownloadAction;

    /**
     * ページのtitleタグに表示する文字列
     *
     * string pageTitle
     */
    protected $pageTitle = 'サンプル管理画面';

    /**
     * ページのh1に表示する文字列
     *
     * string pageNamePrefix
     */
    protected $pageNamePrefix = 'サンプル';

    /**
     * コントローラ名として使用する文字列
     *
     * string controllerName
     */
    protected $controllerName = 'sample';
    
    /**
     * 簡易検索フォームを使用するかどうか
     *
     * boolean searchShortcut
     */
    protected $searchShortcut = true;
    
    /**
     * CSVアップロード・ダウンロード機能の設定
     *
     * array csvSettings
     */
    protected $csvSettings = [
        'ufType' => 'csv1',
        'kind'   => 'sample_csv',
    ];
    
    /**
     * SampleControllerで必要とするエレメント定義を
     * 読み込む
     */
    use SampleElements;

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct(Model $model, Validator $validator)
    {
        parent::__construct();
        
        $this->setCtrlSuppoter(
            $this->getCtrlSuppoterParameters($model, $validator)
        );
    }
    
    /**
     * function getCtrlSuppoterTemplatePrefix
     *
     *
     */
    protected function getCtrlSuppoterTemplatePrefix()
    {
        return 'azuki-sample::manage.sample.sample-';
    }
    
    /**
     *
     * CSVダウンロード処理を実装したサービスを返すメソッド
     *
     */
    public function getCsvDownloadService()
    {
        return app(\AzukiSample\App\Services\Downloader\SampleCsvDownloader::class);
    }
    
    /**
     *
     * CSVダウンロードファイル名を返すメソッド
     *
     */
    public function getCsvDownloadFileName()
    {
        return sprintf('sampleList_%s.csv', date('Y-m-d_His'));
    }
}
