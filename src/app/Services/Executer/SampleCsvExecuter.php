<?php
namespace AzukiSample\App\Services\Executer;
/**
 * 通常ファイルのアップロード処理を行うクラス
 *
 *
 * @copyright Copyright 2018 Azuki Project.
 * @author    Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */
use Azuki\App\Services\Executer\Abstracts\CsvExecuter;
use Azuki\App\Services\ExtendLog;

use AzukiSample\App\Http\Validation\Manage\SampleValidator;
use AzukiSample\App\Models\Sample;
use AzukiSample\App\Http\Modules\ControllerElements\Sample as SampleElements;

class SampleCsvExecuter extends CsvExecuter
{
    /**
     * SampleControllerで必要とするエレメント定義を
     * 読み込む
     */
    use SampleElements;

    /**
     *
     *
     */
    protected $columns;

    /**
     *
     *
     */
    protected $converts = [
        'zipcode1' => 'cnvZipcode1',
        'zipcode2' => 'cnvZipcode2',
        'birth'    => 'cnvBirth',
    ];
    
    /**
     * function getValidator
     *
     * バリデーションオブジェクトを返す
     * 将来的にバリデーションを複数管理できるように拡張したいので、その時にはここで
     * どのバリデーションを返すかという部分を吸収させる
     *
     */
    protected function getValidator($data)
    {
        $data['belong'] = $this->belong;
        $id   = isset($data['id']) && !empty($data['id']) ? $data['id'] : null;
        $flow = !is_null($id) ? 'edit' : 'regist';
        $validator = new SampleValidator();
        
        // バリデーションの初期化を行う
        // このサンプル機能ではバリデーションクラスが自前でルールメソッドを持ち
        // 設定からのルール生成をしていないので不要なのだけど、サンプルなので記載
        // 設定にてルール生成しているコントローラと同様にバリデーション処理する場合に必要な処理
        $this->elements = $this->loadModuleElements();
        $validator->setValidInitialize($this->getValidSettings());
        
        return $validator->getValidator($data, $flow, $id);
    }
    
    /**
     *
     *
     */
    protected function convertLineToData($data)
    {
        $ret = [];
        
        $data = $data[0];
        $columns = $this->getColumns();
        foreach( $columns as $row ) {
            $key = $row['key'];
            $ret[$row['column']] = $data[$key];
            if( isset($row['convert']) && !empty($row['convert']) ) {
                $ret[$row['column']] = call_user_func([$this, $row['convert']], $data[$key]);
            }
        }
        
        // パスワードを固定で追加
        if(empty($ret['id'])) {
            $ret['password']      = '12345678';
            $ret['password_conf'] = '12345678';
        }
        
        return $ret;
    }
    
    /**
     *
     *
     *
     */
    protected function getColumns()
    {
        if( empty($this->columns) ) {
            $column = app(BASE_APP_ACCESSOR)->get('execute_type.sample_csv.columns');
            $columns = [];
            
            foreach( $column as $key => $row ) {
                $cols = is_array($row['column']) ? $row['column'] : [$row['column']];
                foreach( $cols as  $col) {
                    $convert = isset($this->converts[$col]) ? $this->converts[$col] : '';
                    $columns[] = [
                        'key'     => $key,
                        'column'  => $col,
                        'convert' => $convert,
                    ];
                }
            }
            $this->columns = $columns;
        }

        return $this->columns;
    }
    
    /**
     *
     *
     *
     */
    protected function individualProcess($res, $data)
    {
        $model = $this->getModel();
        $id    = isset($data['id']) ? $data['id'] : null;
        try {
            $obj = $model->prepare()->find($id);
            if( is_null($obj) ) {
                if(method_exists($model, 'setBelongValue')) {
                    $model->setBelongValue($this->belong);
                }
                $ret = $model->_create($data);
            } else {
                $ret = $model->_update($id, $data);
            }
            if( $ret === false ) {
                throw new \Exception("Sampleモデルにて登録に失敗[ExceptionLogを参照してください]。\n");
            }
            $res->successCountUp();
        } catch(\Exception $e) {
            $res->failCountUp();
            $res->putFails($e->getMessage());
        }
        
        return $res;
    }
    
    /**
     *
     *
     */
    protected function getModel()
    {
        return new Sample();
    }
    
    /**
     *
     *
     */
    public function cnvZipcode1($val)
    {
        return substr($val, 0, 3);
    }
    
    /**
     *
     *
     */
    public function cnvZipcode2($val)
    {
        $haifun = ['-', 'ー', '―', '－', '‐'];
        $zip = str_replace($haifun, '', $val);
        
        return substr($zip, 3, 4);
    }
    
    /**
     *
     *
     */
    public function cnvBirth($val)
    {
        if( FALSE !== strpos( str_replace('　', ' ', $val), ' ') ) { // とりあえずの処理。正規表現でチェックの方がよいか。
            $val = $val. ' 00:00:00';
        }
        return date('Y-m-d', strtotime($val));
    }
}
