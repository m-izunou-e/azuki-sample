<?php
namespace AzukiSample\App\Services\Downloader;

/**
 * 通常ファイルのアップロード処理を行うクラス
 *
 *
 * @copyright Copyright 2018 Azuki Project.
 * @author    Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */
use Azuki\App\Services\Downloader\Abstracts\CsvDownloader;
use Azuki\App\Services\ExtendLog;

use AzukiSample\App\Models\Sample;

class SampleCsvDownloader extends CsvDownloader
{
    /**
     *
     *
     */
    protected $headerLine;

    /**
     *
     *
     */
    protected function getHeaderLine()
    {
        if( empty($this->headerLine) ) {
            $columns = app(BASE_APP_ACCESSOR)->get('execute_type.sample_csv.columns');
            $headers = [];
            
            foreach( $columns as $row ) {
                $col = isset($row['colName']) ? $row['colName'] : $row['column'];
                $headers[$col] = $row['header'];
            }
            $this->headerLine = $headers;
        }

        return $this->headerLine;
    }
    
    /**
     *
     *
     */
    protected function getSqlForOutputCsvData()
    {
        $model = $this->getModel();
        return $model->getSqlForOutputCsvData( $this->getCsvSearchCondition() );
    }
    
    /**
     *
     *
     */
    protected function convertOutputData($row, $before)
    {
        $data = [];
        
        $cols = array_keys($this->getHeaderLine());
        foreach($cols as $col) {
            $data[$col] = $row->{$col};
        }
        
        return [$data];
    }
    
    /**
     *
     *
     */
    protected function getModel()
    {
        return new Sample();
    }
}
