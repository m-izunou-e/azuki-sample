<?php
namespace AzukiSample\App\Contracts\Models;

/**
 * 
 *
 * @copyright Copyright 2018 Azuki Project.
 * @author    Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */
use Azuki\App\Contracts\Models\BaseModel;

/**
 * class Sample
 *
 */
interface Sample extends BaseModel
{
}
