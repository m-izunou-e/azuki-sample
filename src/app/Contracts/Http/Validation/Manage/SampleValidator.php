<?php
namespace AzukiSample\App\Contracts\Http\Validation\Manage;

/**
 * 
 *
 * @copyright Copyright 2018 Azuki Project.
 * @author    Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */
use Azuki\App\Contracts\Http\Validation\Validator;

/**
 * class ManagersValidator
 *
 */
interface SampleValidator extends Validator
{
}
