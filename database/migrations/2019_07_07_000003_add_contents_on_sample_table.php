<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddContentsOnSampleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('azuki_sample', function (Blueprint $table) {
            $table->mediumText('contents')->nullable()->after('enable')->comment('wisywigエディタコンテンツ(Json)');
            $table->mediumText('contents_html')->nullable()->after('contents')->comment('wisywigエディタコンテンツ(HTML)');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('azuki_sample', function (Blueprint $table) {
            $table->dropColumn('contents');
            $table->dropColumn('contents_html');
        });
    }
}
