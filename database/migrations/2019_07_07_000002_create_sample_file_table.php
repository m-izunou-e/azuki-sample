<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSampleFileTable extends Migration
{
    protected $table = 'azuki_sample_file';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->table, function (Blueprint $table) {
            $table->engine = 'InnoDB ROW_FORMAT=DYNAMIC';

            $table->bigIncrements('id')->unsigned()->comment('一意の識別子');
            $table->bigInteger('sample_id')->unsigned()->index()->comment('紐づくsampleのID');
            $table->string('file_id_name', 255)->index()->comment('紐づくfile_infoのID_NAME');
            $table->string('column_name', 64)->comment('紐づくカラムの名前');
            $table->integer('order')->unsigned()->comment('ファイルの順番');
            $table->timestampsTz();

            $table->foreign('sample_id')
                ->references('id')->on('azuki_sample');
            
        });
        DB::statement("ALTER TABLE `".$this->table."` COMMENT 'sampleに紐づくアップロードファイル管理テーブル'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->table);
    }
}
