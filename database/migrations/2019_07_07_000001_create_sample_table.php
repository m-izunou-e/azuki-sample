<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSampleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('azuki_sample', function (Blueprint $table) {
            $table->engine = 'InnoDB ROW_FORMAT=DYNAMIC';

            $table->bigIncrements('id')->unsigned()->comment('一意の識別子');
            $table->string('name', 36)->index()->comment('ユーザー名。最大32文字');
            $table->string('email', 255)->index()->comment('メールアドレス');
            $table->string('password', 200)->comment('パスワード。英数記号大文字小文字混在６文字以上32文字以内。bcryptでハッシュ化');
            $table->integer('zipcode')->unsigned()->index()->comment('郵便番号');
            $table->integer('pref')->unsigned()->index()->comment('都道府県');
            $table->string('address1', 255)->comment('市区町村');
            $table->string('address2', 255)->nullable()->comment('番地・マンション名など');
            $table->smallInteger('gender')->unsigned()->index()->comment('性別');
            $table->date('birth')->nullable()->comment('生年月日');
            $table->smallInteger('role')->unsigned()->index()->comment('権限番号');
            $table->smallInteger('is_login')->unsigned()->index()->default(0)->comment('ログイン可否。可能：1．不可：2');
            $table->smallInteger('enable')->unsigned()->index()->default(0)->comment('有効・無効。有効：1．無効：2');
            $table->softDeletesTz();
            $table->timestampsTz();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('azuki_sample');
    }
}
