@extends($vendorPrefix.'layouts.share_detail',['baseLayout' => 'manage_simple'])

@if(intval(app(BASE_APP_ACCESSOR)->get('standard.version')) >= '4.1')
@section('modalContents')
@parent
  @include ($vendorPrefix."layouts.parts.share.preview-modal")
@stop
@endif
